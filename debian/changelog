fiat (2019.2.0~git20210419.7d418fa-3) unstable; urgency=medium

  * debian patch fix_deprecated_numpy.patch replaces deprecated
    np.float with float. Closes: #1027195.
  * debian/tests: skip test_serendipity_derivatives, it fails now in
    lambdify function (tabulate)
  * Standards-Version: 4.6.2

 -- Drew Parsons <dparsons@debian.org>  Mon, 09 Jan 2023 02:22:32 +0100

fiat (2019.2.0~git20210419.7d418fa-2) unstable; urgency=medium

  * add debian patches
    - reproducible-build.patch links docs to local python docs to
      enable reproducible builds. Thanks Chris Lamb. Closes: #1007908.
    - docs_local_mathjax.patch configures docs to use local mathjax
  * Standards-Version: 4.6.1

 -- Drew Parsons <dparsons@debian.org>  Sun, 26 Jun 2022 20:54:41 +0200

fiat (2019.2.0~git20210419.7d418fa-1) unstable; urgency=medium

  * New upstream snapshot
  * mark python-fiat-doc as Multi-Arch: foreign
  * only reprocess mathjax links if any are found. Closes: #997747.
  * Standards-Version: 4.6.0

 -- Drew Parsons <dparsons@debian.org>  Sun, 24 Oct 2021 20:42:19 +0200

fiat (2019.2.0~git20210116.0439689-2) unstable; urgency=medium

  * upload latest snapshot to unstable

 -- Drew Parsons <dparsons@debian.org>  Tue, 09 Feb 2021 20:39:05 +0100

fiat (2019.2.0~git20210116.0439689-1) experimental; urgency=medium

  * New upstream snapshot.
  * Provide documentation in python-fiat-doc.
  * Remove deprecated get-orig-source rule from debian/rules.
  * Standards-Version: 4.5.1

 -- Drew Parsons <dparsons@debian.org>  Mon, 25 Jan 2021 19:44:21 +0100

fiat (2019.2.0~git20200919.42ceef3-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Drew Parsons <dparsons@debian.org>  Wed, 30 Sep 2020 17:08:28 +0800

fiat (2019.2.0~git20200722.e16d9f3-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Drew Parsons <dparsons@debian.org>  Sat, 25 Jul 2020 20:00:05 +0800

fiat (2019.2.0~git20200609.ccc94c3-1) unstable; urgency=medium

  * New upstream snapshot.
  * debhelper compatibility level 13

 -- Drew Parsons <dparsons@debian.org>  Thu, 11 Jun 2020 14:44:43 +0800

fiat (2019.2.0~git20191021.825b23a-2) unstable; urgency=medium

  * drop debian patch fix_tests_pytest4.patch (applied upstream)
  * ensure debian/tests catches test errors
  * separate unit tests from regression tests in debian/tests
    - apply Restrictions: needs-internet to test-fiat-regression in
      debian/tests to download reference data for regression tests.

 -- Drew Parsons <dparsons@debian.org>  Mon, 20 Apr 2020 01:07:44 +0800

fiat (2019.2.0~git20191021.825b23a-1) experimental; urgency=medium

  * New upstream snapshot for dolfinx
  * update upstream source repo to https://github.com/FEniCS/fiat
  * Standards-Version: 4.5.0
  * drop dummy package python-fiat

 -- Drew Parsons <dparsons@debian.org>  Fri, 24 Jan 2020 02:30:01 +0800

fiat (2019.1.0-3) unstable; urgency=medium

  * debian patch fix_tests_pytest4.patch adapts upstream PR#31 and
    PR#37 to fix tests for pytest4. Closes: #942237.
  * Standards-Version: 4.4.1

 -- Drew Parsons <dparsons@debian.org>  Wed, 23 Oct 2019 02:12:10 +0800

fiat (2019.1.0-2) unstable; urgency=medium

  * Standards-Version: 4.4.0
  * drop python-fiat

 -- Drew Parsons <dparsons@debian.org>  Wed, 31 Jul 2019 12:00:22 +0800

fiat (2019.1.0-1) experimental; urgency=medium

  * New upstream release (official).
  * debhelper 12: Build-Depends: debhelper-compat (= 12)

 -- Drew Parsons <dparsons@debian.org>  Thu, 25 Apr 2019 10:33:13 +0800

fiat (2019.1.0~git20190220-1) experimental; urgency=medium

  * New upstream test release.
    - applies patches fix_sympy1.2_8ceb29a.patch
      and deprecate_numpy_matrix_812395a.diff

 -- Drew Parsons <dparsons@debian.org>  Wed, 06 Mar 2019 02:46:10 +0800

fiat (2018.1.0-5) unstable; urgency=medium

  * add debian patch deprecate_numpy_matrix_812395a.diff
    (upstream commit 812395ae3f7daa1c2b1b374ddf4d0794ba07393b)
    to stop using deprecated numpy.matrix
  * Standards-Version: 4.3.0
  * debhelper compatibility level 12

 -- Drew Parsons <dparsons@debian.org>  Wed, 30 Jan 2019 18:33:19 +0800

fiat (2018.1.0-4) unstable; urgency=medium

  * debian/tests (autopkgtest) Depends: ca-certificates

 -- Drew Parsons <dparsons@debian.org>  Mon, 30 Jul 2018 17:13:30 +0800

fiat (2018.1.0-3) unstable; urgency=medium

  * don't use fresh source for autopkgtest (debian/tests)
  * use DATA_REPO_GIT="" for tests to download fiat-reference-data
  * don't run autopkgtest with MPI (confuses git when updating
    fiat-reference-data, and FIAT is not directly MPI-enabled)
  * redirect test stderr to test.err unless there is an actual error
    (stderr must be empty for autopkgtest)

 -- Drew Parsons <dparsons@debian.org>  Thu, 26 Jul 2018 17:55:39 +0800

fiat (2018.1.0-2) unstable; urgency=medium

  * activate autopkgtest via debian/tests

 -- Drew Parsons <dparsons@debian.org>  Thu, 26 Jul 2018 00:06:42 +0800

fiat (2018.1.0-1exp2) experimental; urgency=medium

  * Standards-Version: 4.1.5
  * apply sympy 1.2 patch to 2018.1.0

 -- Drew Parsons <dparsons@debian.org>  Tue, 17 Jul 2018 08:38:26 +0800

fiat (2018.1.0-1exp1) experimental; urgency=medium

  [ Nico Schlömer ]
  * remove Python 2 packages in preparation of 2018.1

  [ Drew Parsons ]
  * New upstream version.
    - python-fiat is now a dummy package depending on python3-fiat
  * remove ancient-python-version-field X-Python3-Version: >= 3.4

 -- Drew Parsons <dparsons@debian.org>  Fri, 15 Jun 2018 00:30:56 +0200

fiat (2017.2.0.0-4) unstable; urgency=medium

  * Upstream patch fix_sympy1.2_8ceb29a.patch fixes regression in FIAT
    under sympy 1.2.

 -- Drew Parsons <dparsons@debian.org>  Tue, 17 Jul 2018 08:27:48 +0800

fiat (2017.2.0.0-3) unstable; urgency=medium

  * Depends: python3-pkg-resources. Closes: #896415, #896416.
  * Standards-Version: 4.1.4

 -- Drew Parsons <dparsons@debian.org>  Sun, 22 Apr 2018 11:47:27 +0800

fiat (2017.2.0.0-2) unstable; urgency=medium

  * debian/control: Update VCS tags to salsa.debian.org

 -- Drew Parsons <dparsons@debian.org>  Wed, 21 Feb 2018 22:20:33 +0800

fiat (2017.2.0.0-1exp1) experimental; urgency=medium

  * New upstream release
    - This is the official 2017.2.0 release. The previous version was a
      mislabelled beta release.
  * Standards-Version: 4.1.3
  * debhelper compatibility level 11
  * debian/control Priority: optional not extra.

 -- Drew Parsons <dparsons@debian.org>  Fri, 19 Jan 2018 23:48:09 +0800

fiat (2017.2.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
    - updated debian/upstream/signing-key.asc for signature
      from the FEniCS Project Steering Council (key BED06106DD22BAB3)
  * Standards-Version: 4.1.1

 -- Drew Parsons <dparsons@debian.org>  Sat, 07 Oct 2017 16:03:24 +0800

fiat (2017.1.0-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.1.0
  * debhelper compatibility level 10

 -- Drew Parsons <dparsons@debian.org>  Sat, 09 Sep 2017 14:32:06 +0800

fiat (2017.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Disable testing.

 -- Johannes Ring <johannr@simula.no>  Wed, 10 May 2017 11:51:19 +0200

fiat (2016.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Johannes Ring ]
  * Support Python 3: new package python3-fiat.
  * Enable testing.

 -- Drew Parsons <dparsons@debian.org>  Thu, 19 Jan 2017 22:35:03 +0800

fiat (2016.2.0-1) unstable; urgency=medium

  * New upstream release.
  * d/watch: Check pgp signature.
  * d/control: Remove Christophe Prud'homme from Uploaders (closes: #835008).

 -- Johannes Ring <johannr@simula.no>  Thu, 01 Dec 2016 12:18:18 +0100

fiat (2016.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Fri, 24 Jun 2016 12:59:43 +0200

fiat (1.6.0-2) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Update VCS fields after the move to Git.
  * Standards-Version: 3.9.8

 -- Drew Parsons <dparsons@debian.org>  Thu, 28 Apr 2016 19:37:34 +0800

fiat (1.6.0-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Tue, 11 Aug 2015 10:09:09 +0200

fiat (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.6.
    - Bump X-Python-Version to >= 2.7.
    - Replace python-scientific with python-sympy in Depends field.
    - Bump python-all in Build-Depends to >= 2.7.

 -- Johannes Ring <johannr@simula.no>  Mon, 12 Jan 2015 19:33:29 +0100

fiat (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Bump Standards-Version to 3.9.5.

 -- Johannes Ring <johannr@simula.no>  Tue, 03 Jun 2014 18:11:38 +0200

fiat (1.3.0-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Update URL for move to Bitbucket.

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Jun 2013 12:39:58 +0200

fiat (1.1-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Replace http with https in URL.
  * debian/control:
    - Bump Standards-Version to 3.9.4.
    - Remove DM-Upload-Allowed field.
    - Bump required debhelper version in Build-Depends.
    - Remove cdbs from Build-Depends.
    - Use canonical URIs for Vcs-* fields.
  * debian/compat: Bump to compatibility level 9.
  * debian/rules: Rewrite for debhelper (drop cdbs).

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Jun 2013 12:23:49 +0200

fiat (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Update Homepage field.

 -- Johannes Ring <johannr@simula.no>  Wed, 07 Dec 2011 14:38:57 +0100

fiat (1.0-beta-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Mon, 15 Aug 2011 16:58:34 +0200

fiat (0.9.9-2) unstable; urgency=low

  * Move from python-central to dh_python2 (closes: #616808).
    - Remove python-central from Build-Depends.
    - Bump minimum required python-all package version to 2.6.6-3~.
    - Remove XB-Python-Version line.
    - Bump minimum required cdbs version to 0.4.90~.
    - Remove DEB_PYTHON_SYSTEM=pycentral from debian/rules.
    - Replace XS-Python-Version with X-Python-Version.
  * Remove old fields Conflicts, Provides, and Replaces from
    debian/control.
  * Bump Standards-Version to 3.9.2 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Wed, 22 Jun 2011 12:50:40 +0200

fiat (0.9.9-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1 (no changes needed).
  * Update Homepage field in debian/control.

 -- Johannes Ring <johannr@simula.no>  Wed, 23 Feb 2011 23:01:08 +0100

fiat (0.9.2-1) unstable; urgency=low

  * New upstream release.
  * Package moved from pkg-scicomp to Debian Science.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Bump Standards-Version to 3.9.0 (no changes needed).
  * Remove patch (added upstream).

 -- Johannes Ring <johannr@simula.no>  Thu, 01 Jul 2010 13:57:35 +0200

fiat (0.9.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Add python-scientific to Depends.
    - Bump Standards-Version to 3.8.4 (no changes).
  * debian/copyright: Update to reflect new files.
  * debian/watch: Update download URL.
  * debian/rules: get-orig-source target no longer repacks orig tarball.
  * Add patch to fix error wrong-path-for-interpreter reported by lintian.

 -- Johannes Ring <johannr@simula.no>  Wed, 03 Feb 2010 17:49:28 +0100

fiat (0.3.5-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright to reflect new files.
  * debian/control:
    - Add DM-Upload-Allowed: yes.
    - Bump Standards-Version to 3.8.3 (no changes needed).
    - Bump debhelper version to 7.
  * debian/compat: Bump dh compat to 7.
  * debian/watch, debian/copyright: Update download URLs.

 -- Johannes Ring <johannr@simula.no>  Fri, 11 Dec 2009 09:28:12 +0100

fiat (0.3.4-1) unstable; urgency=low

  * Initial release (Closes: #503077)

 -- Johannes Ring <johannr@simula.no>  Sat, 13 Sep 2008 14:02:28 +0200

